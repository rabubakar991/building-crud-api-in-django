from rest_framework import serializers
from .models import MovieList
# from abc import abstractmethod

# def name_lenght(value):
#     if len(value)<2:
#         raise serializers.ValidationError('Name is too short')

# class MovieSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     name = serializers.CharField(validators=[name_lenght])
#     description = serializers.CharField()
#     status = serializers.BooleanField()
#
#     @abstractmethod
#     def create(self, validated_data):
#         return MovieList.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.name = validated_data('name', instance.name)
#         instance.description = validated_data('description', instance.description)
#         instance.status = validated_data('status', instance.status)
#         instance.save()
#         return instance


class MovieSerializer(serializers.ModelSerializer):
    NameLength = serializers.SerializerMethodField()

    class Meta:
        model = MovieList
        fields = '__all__'

    def get_NameLength(self, object):
        lentgh = len(object.name)
        return lentgh

    def validate(self, data):
        if data['name'] == data['description']:
            raise serializers.ValidationError('Description must be different form Title.')
        else:
            return data

    def validate_name(self, value):
        if len(value) < 2:
            raise serializers.ValidationError('Name is too short')
        else:
            return value
