from .serializers import MovieSerializer
from rest_framework.response import Response
from .models import MovieList
from rest_framework.views import APIView
from rest_framework import status


# Create your views here.
class ViewMovieList(APIView):
    def get(self, request):
        movies = MovieList.objects.all()
        serializer = MovieSerializer(movies, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = MovieSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)


class ViewMovieDetail(APIView):
    def get(self, request, pk):
        movies = MovieList.objects.get(pk=pk)
        serializer = MovieSerializer(movies)
        return Response(serializer.data)

    def put(self, request, pk):
        movie = MovieList.objects.get(pk=pk)
        serializer = MovieSerializer(movie, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        movie = MovieList.objects.get(pk=pk)
        movie.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


"""FUNCTON BASED VIEW CRUD OPERATION"""
# @api_view(['GET', 'POST'])
# def movie_list(request):
#     try:
#         if request.method == 'GET':
#             movies = MovieList.objects.all()
#             serializer = MovieSerializer(movies, many=True)
#             return Response(serializer.data)
#         if request.method == 'POST':
#             serializer = MovieSerializer(data=request.data)
#             if serializer.is_valid():
#                 serializer.save()
#                 return Response(serializer.data)
#             else:
#                 return Response(serializer.errors)
#     except Exception as e:
#         return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
#
#
# @api_view(['GET', 'PUT', 'DELETE'])
# def movie_detail(request, pk):
#     try:
#         if request.method == 'GET':
#             movies = MovieList.objects.get(pk=pk)
#             serializer = MovieSerializer(movies)
#             return Response(serializer.data)
#         if request.method == 'PUT':
#             movie = MovieList.objects.get(pk=pk)
#             serializer = MovieSerializerNew(movie, data=request.data)
#             if serializer.is_valid():
#                 serializer.save()
#                 return Response(serializer.data)
#             else:
#                 return Response(serializer.errors)
#         if request.method == 'DELETE':
#             movie = MovieList.objects.get(pk=pk)
#             movie.delete()
#             return Response(status=status.HTTP_204_NO_CONTENT)
#     except Exception as e:
#         return Response({'error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
