# Generated by Django 4.0.1 on 2022-02-07 06:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('watch_app', '0002_rename_title_moviemodel_name_and_more'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='MovieModel',
            new_name='MovieList',
        ),
    ]
