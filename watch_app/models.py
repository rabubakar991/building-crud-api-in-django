from django.db import models


# Create your models here.
class MovieList(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)
    status = models.BooleanField(default=True)
    age = models.CharField(max_length=100)

    def __str__(self):
        return self.name
