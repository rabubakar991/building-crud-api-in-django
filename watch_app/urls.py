from django.urls import path
from . import views

urlpatterns = [
    path('', views.ViewMovieList.as_view(), name='movie_list'),
    path('<int:pk>', views.ViewMovieDetail.as_view(), name='movie_detail'),
]
